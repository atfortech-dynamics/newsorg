package com.atfortecdynamics.newsorg.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.atfortecdynamics.newsorg.DO.Article;
import com.atfortecdynamics.newsorg.R;
import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by folio on 10/25/2018.
 */

public class ListViewAdapter extends BaseAdapter {

    /* this context allows you to load this class into an activity that is c
        currently active
     */
    Context context;

    /* this is the data that we want to load to our adapter in order
    to pass it to our listview
     */
    List<Article> data;

    /* this is a service whose main function is to pass data to our custom row layout
        it our case R.layout.custom_row
     */
    LayoutInflater inflater;

    public ListViewAdapter(Context context, List<Article> data) {

        this.context = context;
        this.data = data;

        /*Layout inflater service is a service that is responsble for loading linear data in our phones

         */
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        /* our adapter needs to know how many views it is suppose to create
            by specifing the size of our list i.e List<Article> data, we are intentially limit it
            the total size of a list in order to avoid complications
         */
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        /*this is where we create a single list item view, by populating it
            with data from the list

            we create our view of a single list item by inflating it with our custom_row layout
         */
        view = inflater.inflate(R.layout.custom_row,null);
        /*for us to be able to pass data to our view, we need the widgets attached to the layout,
            in our case it will be textview title,textview description and our imageview
         */
        TextView tx_title = view.findViewById(R.id.txt_title);
        TextView  description = view.findViewById(R.id.txt_description);
        ImageView imageView = view.findViewById(R.id.image);

        /*we will now pass data from our List<Article> data to our list
            by using the provided int i (parameter)
         */
        tx_title.setText(data.get(i).getTitle());
        description.setText(data.get(i).getDescription());

        /* for us to be able to load images to our imageView, we need to download the image
            from the given getmageToUrl()
            this would involve using a special jar file called glide that enables us download images
            directly from the server.

                link to gradle file is given below
                 implementation 'com.github.bumptech.glide:glide:4.8.0'
                 annotationProcessor 'com.github.bumptech.glide:compiler:4.8.0'
         */

        Glide.with(context).load(data.get(i).getUrlToImage()).into(imageView);

        /* once our view is populated with data, we return it as a list item view

         */
        return view;
    }
}
