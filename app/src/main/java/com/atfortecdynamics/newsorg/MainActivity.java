package com.atfortecdynamics.newsorg;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.atfortecdynamics.newsorg.Adapters.ListViewAdapter;
import com.atfortecdynamics.newsorg.DO.Article;
import com.atfortecdynamics.newsorg.DO.Source;
import com.atfortecdynamics.newsorg.network.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    String strSearch;
    ListView listView;
    EditText ed_search;
    Button btn_search;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.list_news);
        ed_search=findViewById(R.id.ed_search_headline);
        btn_search=findViewById(R.id.btn_search);

        /* we will set an onclick listner to our button btn_search in order to run our network thread that
            will request for data from the api
         */

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // read the ed_search to see what the user has inputted
                strSearch = ed_search.getText().toString();

                if(strSearch.isEmpty()){
                    Toast.makeText(getApplicationContext(),"please type to search",Toast.LENGTH_SHORT).show();
                }else{
                    // execute the network thread
                    new loadNews().execute();
                }
            }
        });
    }

    // the network thread
    public class loadNews extends AsyncTask<String,String,String>{

        String status;
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);

        /* the on preExecute methods acts like the onPause() activity callback
            where it pauses the activity thread before initiating the
            network thread.
         */
        protected void onPreExecute(){
            super.onPreExecute();
            dialog.setCancelable(false);
            dialog.setMessage("fetching news. Please wait...");
            dialog.show();

        }
        @Override
        protected String doInBackground(String... strings) {
            JSONParser jsonParser = new JSONParser();
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("apiKey",Config.API_KEY));
            params.add(new BasicNameValuePair("q",strSearch));

            JSONObject jsonObject = jsonParser.makeHttpRequest(Config.hostmane+
            Config.top_headline,params);

            //read our json file
            try {
                status=jsonObject.getString("status");
                JSONArray data = jsonObject.getJSONArray("articles");

                if(status.equals("ok")){
                    /* since the json array has been found, we execute our ArrayDecoder
                        in order to add data to our Config.articleList

                        this is done by creating a new object of ArrayDecoder.class
                     */
                    new ArrayDecoder(data).loadList();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String s){
            super.onPostExecute(s);
            if(status.equals("ok")){
                /* if the status is okay, it means our Config.artcleList has data in it
                    thus we will create an Object of ListViewAdapter which we will use to
                    populate our listview with data
                 */
                ListViewAdapter adapter = new ListViewAdapter(MainActivity.this,Config.articleList);

                listView.setAdapter(adapter);
            }else Toast.makeText(getApplicationContext(),status,Toast.LENGTH_SHORT).show();

            dialog.dismiss();
        }
    }

    public class ArrayDecoder{
        JSONArray jsonArray;

        public ArrayDecoder(JSONArray jsonArray) {
            this.jsonArray = jsonArray;
        }

        public void loadList() throws JSONException {
            Config.articleList = new ArrayList<>();

            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                JSONObject jSource = jsonObject.getJSONObject("source");
                Source source = new Source();
                source.setId(jSource.getString("id"));
                source.setName(jSource.getString("name"));

                Article article = new Article();
                article.setAuthor(jsonObject.getString("author"));
                article.setTitle(jsonObject.getString("title"));
                article.setContent(jsonObject.getString("content"));
                article.setDescription(jsonObject.getString("description"));
                article.setUrl(jsonObject.getString("url"));
                article.setUrlToImage(jsonObject.getString("urlToImage"));
                article.setPublishedAt(jsonObject.getString("publishedAt"));

                article.setSource(source);

                Config.articleList.add(article);

            }
        }

    }
}
